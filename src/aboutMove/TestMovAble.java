package aboutMove;

public class TestMovAble {
    public static void main(String[] args) {
        MovAble m1 = new MovAblePoint(5, 6, 10, 15);     // upcast
        System.out.println(m1);
        m1.MoveLeft();
        System.out.println(m1);

        MovAble m2 = new MovableCircle(1, 2, 3, 4, 20);  // upcast
        System.out.println(m2);
        m2.MoveRight();
        System.out.println(m2);
    }
}
