package aboutMove;

public class MovableCircle implements MovAble {
    private int radius;
    private MovAblePoint center;


    public MovableCircle(int x,int y, int xSpeed, int ySpeed,int radius){
        center = new MovAblePoint(x,y,xSpeed,ySpeed);
        this.radius = radius;
    }
    @Override
    public String toString() {
        return super.toString();
    }


    @Override
    public void MoveDown() {

        center.y+=center.ySpeed;
    }

    @Override
    public void MoveUp() {
        center.y -= center.ySpeed ;
    }
    @Override
    public void MoveLeft() {
        center.x-= center.xSpeed;
    }

    @Override
    public void MoveRight() {
        center.x+= center.xSpeed;
    }


}
