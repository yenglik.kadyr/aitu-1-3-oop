package aboutMove;

public interface MovAble {
    public void MoveUp();
    public void MoveDown();
    public void MoveLeft();
    public void MoveRight();
}
