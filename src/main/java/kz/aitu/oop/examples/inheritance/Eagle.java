package kz.aitu.oop.examples.inheritance;

public class Eagle extends Bird {
    private String name = "eagle";
    private int lifespan = 15;

    public Eagle(String reproduction, String outerCovering, String name, int lifespan) {
        super(reproduction, outerCovering);
        this.name = name;
        this.lifespan = lifespan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifespan() {
        return lifespan;
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
    }
}
