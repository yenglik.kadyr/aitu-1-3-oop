package kz.aitu.oop.examples.assignment3;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        readFromFile();

    }

    public static void writeToFile() throws IOException {
        MyString myString = new MyString(new int[]{1,2,3,3,4,4,4,4,5});

        String filename = "";
        Scanner scanner = new Scanner(System.in);
        filename = scanner.next();

        if(filename.isEmpty()) return;

        File file = new File(filename);
        if(file.createNewFile()) {
            FileWriter fileWriter = new FileWriter(file);

            for (int i = 0; i < myString.length(); i++) {
                fileWriter.write(myString.valueAt(i) + "\n");
            }
            fileWriter.close();
        }
    }

    public static void readFromFile() throws IOException {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.next();

        if(filename.isEmpty()) return;


        File file = new File(filename);
        Scanner sc2 = new Scanner(file);

        int array[] = new int[10];
        int k = 0;
        while (sc2.hasNextInt()) {
            array[k] = sc2.nextInt();
            k++;
        }

        MyString myString = new MyString(array);
        myString.print();
    }
}
