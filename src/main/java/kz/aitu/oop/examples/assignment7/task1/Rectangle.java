package kz.aitu.oop.examples.assignment7.task1;

public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle() {
        width = 1.0;
        length = 1.0;
    }

    public Rectangle(String color, boolean filled, double width, double length) {
        super(filled, color);
        this.length = length;
        this.width = width;
    }
}
