package kz.aitu.oop.examples.assignment7.task1;

public class Square extends  Rectangle{
    private double side;

    public Square(String color, boolean filled, double width, double length, double side) {
        super(color, filled, width, length);
    }

    public Square(double width, double length) {
        super(width, length);
    }

    public Square() {
        setWidth(2.0);
        setLength(2.0);
    }


    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public String toString() {
        if (isFilled() == true) {
            return "A Square with side = " + getWidth() + " , which is a subclass of A Rectangle with width = "
                    + getWidth() + " and length = " + getLength() + " , which is a subclass of A shape with color of "
                    + getColor() + " and filled";
        } else {
            return "A Square with side = " + getWidth() + " , which is a subclass of A Rectangle with width = "
                    + getWidth() + " and length = " + getLength() + " , which is a subclass of A shape with color of "
                    + getColor() + " and Not filled";
        }
    }


    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public String toString() {
        if(isFilled() == true){
            return "A Rectangle with width = " + width +" and length = " + length + " , which is a subclass of A shape with color of "
                    + getColor() + " and filled";
        }else{
            return "A Rectangle with width = " + width +" and length = " + length + " , which is a subclass of A shape with color of "
                    + getColor() + " and Not filled";
        }
    }
}





