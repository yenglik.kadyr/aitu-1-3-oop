package kz.aitu.oop.examples.assignment7.task2;

public interface Movable {
    public void MoveUp();
    public void MoveDown();
    public void MoveLeft();
    public void MoveRight();
}
