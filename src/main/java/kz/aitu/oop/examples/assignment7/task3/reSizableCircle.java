package kz.aitu.oop.examples.assignment7.task3;

public abstract class reSizableCircle  extends Circle implements reSizable {


    public reSizableCircle(double radius) {
        super(radius);
    }

    @Override
    public double getPerimeter() {
        return getPerimeter();
    }

    @Override
    public double getArea() {
        return getArea();
    }

    @Override
    public double resize() {
        return 0;
    }
}

