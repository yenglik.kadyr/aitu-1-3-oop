package kz.aitu.oop.examples.assignment7.task2;

public class MovablePoint {


    public class MovablePoint implements Movable{
        public int ySpeed;
        public int xSpeed;
        public int x;
        public int y;


        public MovablePoint(int x, int y,int xSpeed,int ySpeed ) {
            this.x = x;
            this.y = y;
            this.xSpeed = xSpeed;
            this.ySpeed = ySpeed;
        }

        @Override
        public String toString(){
            return "Point at(" + x + ", " + y + ")";
        }

        @Override
        public void MoveUp() {
            y-= ySpeed;
        }

        @Override
        public void MoveDown() {
            y+= ySpeed;
        }

        @Override
        public void MoveLeft() {
            x-= xSpeed;
        }

        @Override
        public void MoveRight() {
            x+= xSpeed;
        }

    }


}

