package kz.aitu.oop.examples.practice.pract5week7;

public class Main {

    public static void main(String[] args) {
        Stone.SemiPrecious ss = new Stone.SemiPrecious();
        Stone.Precious pp = new Stone.Precious();

        double totalSS = 0, massSS = 0;
        double totalPP = 0, massPP = 0;

        massPP += pp.getMass();
        totalPP += pp.getPrice();

        massSS += ss.getMass();
        totalSS += ss.getPrice();

        System.out.println("Total price of precious stone: " + totalSS + " KZT" + ", mass: " + massSS);
        System.out.println("Total price of semiprecious stone: " + totalPP + " KZT" + ", mass: " + massPP);
    }
}