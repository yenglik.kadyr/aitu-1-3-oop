package kz.aitu.oop.examples.practice.practice5;

import java.util.ArrayList;
import java.util.List;

public class Precious extends Stone{

    private List<CatalogofPrecious> catalogofPrecious;

    private List<CatalogofPrecious> catalogofPreciousList;


    public Precious() {

        this.catalogofPreciousList = new ArrayList<>();
    }
    public void addCatalogofPrecious(CatalogofPrecious catalogofPrecious) {
        catalogofPrecious.add(catalogofPrecious);
    }


    public CatalogofPrecious getcostofPrecious ( int costofPrecious){
        for (CatalogofPrecious catalogofPrecious : catalogofPreciousList) {
            if (catalogofPrecious.getCost().equals(costofPrecious)) return catalogofPrecious;
        }
        return null;
    }

    public CatalogofSemiprecious getweightofPrecious (int weightofPrecious){
        for (CatalogofPrecious catalogofPrecious : catalogofPreciousList) {
            if (catalogofPreciousList.getWeight().equals(weightofPrecious)) return catalogofPrecious;
        }
        return null;
    }
}
