package kz.aitu.oop.examples.practice.pract4week7;

public abstract class Accessories {
    public double price;
    public  int numberofAccessories;


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int  getNumberofAccessories() {
        return numberofAccessories;
    }


    public void setNumberofAccessories(int numberofAccessories){
        this.numberofAccessories = numberofAccessories;
    }



    static class AccessoriesforFish extends Accessories{

        @Override
        public double getPrice() {
            return 25000;
        }
        public int getNumberofAccessories () {
            return 15;
        }
    }



    static class AccessoriesforReptile extends Accessories{

        @Override
        public double getPrice() {
            return 15000;
        }

        public int getNumberofAccessories() {
            return 4;
        }
    }

}
