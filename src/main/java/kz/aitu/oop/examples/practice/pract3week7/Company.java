package kz.aitu.oop.examples.practice.pract3week7;

public class Company {
    public double price;
    public  int numberofEmployee;
    public int countofProject;


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int  getNumberofEmployee() {
        return numberofEmployee;
    }


    public void setNumberofEmployee(int numberofEmployee){
        this.numberofEmployee = numberofEmployee;
    }

    public int getCountofProject() {
        return countofProject;
    }

    public void setCountofProject(int countofProject) {
        this.countofProject = countofProject;
    }

    static class Teams extends Company {

        @Override
        public double getPrice() {
            return 250000;
        }
        public int getNumberofEmployee () {
            return 6;
        }
        @Override
        public int getCountofProject() {
            return 10;
        }
    }



    static class CatalogofProject extends Company{

        @Override
        public int getCountofProject() {
            return 10;
        }
        @Override
        public int getNumberofEmployee () {
            return 6;
        }
        @Override
        public double getPrice() {
            return 250000;
        }

    }

}
