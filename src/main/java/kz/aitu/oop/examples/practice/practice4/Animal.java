package kz.aitu.oop.examples.practice.practice4;

import java.util.ArrayList;
import java.util.List;



public class Animal {
    private  List<CatalogofAnimals> catalogofAnimals;
    private List<CatalogofAccessory> catalogofAccessories;

    private List<CatalogofAnimals> catalogofAnimalsList;

    public Animal(){
        this.catalogofAnimalsList = new ArrayList<>();
    }

    public void addCatalogofAnimals(CatalogofAnimals catalogofAnimals) {
        catalogofAnimals.add(catalogofAnimals);
    }

    public CatalogofAnimals getAnimals(int cost){
        for (CatalogofAnimals catalogofAnimals : catalogofAnimalsList){
            if (catalogofAnimals.getCost().equals(cost)) return catalogofAnimals;
        }
        return null;
    }

}