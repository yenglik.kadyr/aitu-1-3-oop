package kz.aitu.oop.examples.practice.practice2;


    public class Wagon extends Train {
        private  int count = 0;
        private double sum = 0;
        private int numberWagon;
        private final double massaWagon = 23000;  // mass of wagon kg
        private final double massaPassenger = 80;  // average weight of passenger
        protected int countPassenger; // count of passengers
        protected double baggage; // mass of baggage

        public int getNumberWagon(){
            return numberWagon;
        }

        public int getCount() {
            return count;
        }

        public Wagon(int numberWagon, int countPassenger, double baggage) {
            this.numberWagon = numberWagon;
            this.countPassenger = countPassenger;
            this.baggage = baggage;
            ++count;
        }


        public int getCountPassenger() {
            return countPassenger;
        }

        public double getBaggage() {
            return baggage;
        }

        @Override
        public double getCarrying() {
            sum = massaWagon + massaPassenger* getCountPassenger() + getBaggage();
            return sum/1000;
        }

        @Override
        public int countPassenger() {
            return getCountPassenger();
        }
    }


