package kz.aitu.oop.examples.practice.practice6.quiz1;


import java.util.Scanner;

    public class Main {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            Singleton singleton = Singleton.getSingleInstance();
            singleton.str = scanner.nextLine();
            Singleton singleton1 = Singleton.getSingleInstance();
            singleton1.str = scanner.nextLine();
            System.out.println(singleton.toString());
            System.out.println(singleton1.toString());
        }
    }

