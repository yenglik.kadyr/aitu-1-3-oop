package kz.aitu.oop.examples.practice.practice2;

public class Locomotive extends Train{

    protected final double massaLocomotive = 130000; // 130 tonna lokomotiva
    private final int controllingPersons = 2; // driver

    public Locomotive() {
        System.out.println("Locomotive = " + massaLocomotive/1000 + " t");
    }

    @Override
    public double getCarrying() {
        return massaLocomotive/1000;
    }

    @Override
    public int countPassenger() {
        return controllingPersons;
    }
}
