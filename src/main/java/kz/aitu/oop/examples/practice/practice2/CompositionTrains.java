package kz.aitu.oop.examples.practice.practice2;
    import java.util.ArrayList;

    public class CompositionTrains {

        private double sumCarrying = 0; //overall mass
        private int countWagon = 0; // count  of wagon
        private int countPeople = 0; // count of people
        private ArrayList<Train> ListTrain = new ArrayList<>();

        public void viewCountWagon() {
            System.out.println("count of wagon = " + countWagon);
        }

        public CompositionTrains(){
        }

        public  void addTrain(Train train){
            if(train instanceof Wagon){
                countWagon +=((Wagon) train).getCount();
            }
            ListTrain.add(train);
        }

        public void calculatingCarrying(){

            for(Train train : ListTrain) {
                sumCarrying+= train.getCarrying();
            }
            System.out.println("overall mass of train  = " +sumCarrying + " t.");
        }

        public void getCountAllPeople(){
            for (Train count: ListTrain){
                countPeople += count.countPassenger();
            }
            System.out.println("Count of passengers(+ drivers) = " +countPeople);
        }

    }

