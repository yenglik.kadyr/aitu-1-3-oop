package kz.aitu.oop.examples.practice.practice6.quiz1;

public class Singleton {
    public static Singleton instance;

    public String str;

    private Singleton() {

    } public static Singleton getSingleInstance (String str) {
        if (instance == null) {
            instance = new Singleton();
            System.out.println("Hello I'm Singleton! Let me say " + str + "Hello " +
                    "World to you");
        }
        return instance; }
}

