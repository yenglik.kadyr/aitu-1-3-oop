package kz.aitu.oop.examples.practice.pract5week7;

public class Stone {
        public double mass;
        public int price;


        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public double getMass() {
            return mass;
        }

        public void setWeight(double mass ){
            this.mass = mass;
        }

        static class SemiPrecious  extends Stone {
            @Override
            public double getMass() {
                return 17.0;
            }



            @Override
            public int getPrice() {
                return 1500000;
            }


        }

        static class Precious extends Stone {
            @Override
            public double getMass() {
                return 17.0;
            }

            @Override
            public int getPrice() {
                return 200000;
            }
        }
    }
