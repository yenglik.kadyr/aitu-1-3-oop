package kz.aitu.oop.examples.practice.practice7.quiz1;

public class FoodFactory {
    public Food getFood(String Order) {

        if ("pizza".equals(Order)) {
            return new Pizza();
        }
        else if ("cake".equals(Order))
        {
            return new Cake();
        }
        return null;
    }

}




