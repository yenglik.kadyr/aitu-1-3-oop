package kz.aitu.oop.examples.practice.practice4;

import java.util.ArrayList;
import java.util.List;


public class CatalogofAnimals {
    private String name;
    private String type;
    private int cost;
    private int numberofAnimals;

    public CatalogofAnimals(String name, String type, int cost, int numberofAnimals) {
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.numberofAnimals = numberofAnimals;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }


    public int getNumberofAnimals() {
        return numberofAnimals;
    }

    public void add(CatalogofAnimals catalogofAnimals) {
    }

    public Object getCost() {
        return cost;
    }

}
