package kz.aitu.oop.examples.practice.practice7.quiz1;

import kz.aitu.oop.examples.practice7week6.Food;
import kz.aitu.oop.examples.practice7week6.FoodFactory;

public class Main {
    public static void main(String args[]) {


        kz.aitu.oop.examples.practice7week6.FoodFactory foodFactory = new FoodFactory();

        Food food = foodFactory.getFood("pizza");

        System.out.println("The factory returned " + food.getClass());
        System.out.println(food.getType());

    }
}



