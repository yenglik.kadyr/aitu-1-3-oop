package Subbbb333;

public interface GeomObj {
    double getPerimeter();
    double getArea();
}
