import Abstracts.ShapE;

public class Circle  extends ShapE {
    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }

    public Circle(){
        radius = 1.0;
    }
    public Circle(String color, boolean filled, double radius){
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        if(isFilled() == true){
            return "A Circle with radius = " + radius + ", which is a subclass of A Shape  with color of " + getColor()
                    + " and filled";
        }
        else {
            return "A Circle with radius = " + radius + ", which is a subclass of A Shape  with color of " + getColor()
                    + " and Not filled";
        }
    }
}
