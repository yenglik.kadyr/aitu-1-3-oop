package Abstracts;


public class SQuare  extends Rectangle {
    private double side;

    public SQuare(String color, boolean filled, double width, double length, double side) {
        super(color, filled, width, length);
    }

    public SQuare(double width, double length) {
        super(width, length);
    }

    public SQuare() {
        setWidth(2.0);
        setLength(2.0);
    }


    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public String toString() {
        if (isFilled() == true) {
            return "A Square with side = " + getWidth() + " , which is a subclass of A Rectangle with width = "
                    + getWidth() + " and length = " + getLength() + " , which is a subclass of A shape with color of "
                    + getColor() + " and filled";
        } else {
            return "A Square with side = " + getWidth() + " , which is a subclass of A Rectangle with width = "
                    + getWidth() + " and length = " + getLength() + " , which is a subclass of A shape with color of "
                    + getColor() + " and Not filled";
        }
    }
}
