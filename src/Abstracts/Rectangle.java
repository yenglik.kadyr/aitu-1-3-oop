package Abstracts;

public class Rectangle extends ShapE {
    private double width;
    private double length;

    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }

    public Rectangle(){
        width  =  1.0;
        length = 1.0;
    }
    public Rectangle(String color, boolean filled, double width, double length ){
    super(filled,color);
    this.length = length;
    this.width = width;

}




    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public String toString() {
        if(isFilled() == true){
            return "A Rectangle with width = " + width +" and length = " + length + " , which is a subclass of A shape with color of "
                    + getColor() + " and filled";
        }else{
            return "A Rectangle with width = " + width +" and length = " + length + " , which is a subclass of A shape with color of "
                    + getColor() + " and Not filled";
        }
    }
}

